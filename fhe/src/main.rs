use std::fs::File;
use pprof::ProfilerGuard;
use concrete::{prelude::*, ClientKey};
use concrete::{Config, generate_keys, set_server_key, ConfigBuilder, FheUint8};
use tracing::info;
use tracing_subscriber;

fn add_fheuint8(a: u8, b: u8, client_key: ClientKey) -> u8 {
    let a: FheUint8 = FheUint8::encrypt(a, &client_key);
    let b: FheUint8 = FheUint8::encrypt(b, &client_key);
    info!("Encrypted data with client key!");

    // Faster to do computation using pointers
    let result: FheUint8 = &a + &b;
    info!("Adding...");
    let decrypted_result: u8 = result.decrypt(&client_key);

    decrypted_result
}
fn main() {
    // install global collector configured based on RUST_LOG env var
    tracing_subscriber::fmt::init();
    // Setup profiler
    let guard: ProfilerGuard = ProfilerGuard::new(100).unwrap();
    // Create a config with uint8 type and default parameters
    let config: Config = ConfigBuilder::all_disabled().enable_default_uint8().build();

    // Generate a key pair. The server_key is public and sent to a server
    // to enable FHE computations
    let (client_key, server_key) = generate_keys(config);
    info!("Generated Keys!");

    // Get a simpler interface to the server key
    set_server_key(server_key);

    let clear_a: u8 = 27u8;
    let clear_b: u8 = 128u8;

    let decrypted_result: u8 = add_fheuint8(clear_a, clear_b, client_key);
    let _clear_result: u8 = clear_a + clear_b;
    info!("Completed!");
    println!("{}", decrypted_result);
    if let Ok(report) = guard.report().build() {
        let file: File = File::create("flamegraph.svg").unwrap();
        report.flamegraph(file).unwrap();

        println!("report: {:?}", &report);
    }
}
